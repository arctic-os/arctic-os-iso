#!/usr/bin/env bash

echo "######################"
echo "Building Arctic OS ISO"
echo "######################"

mkarchiso -v ./archlive || echo "Error building image"

rm -rf work/